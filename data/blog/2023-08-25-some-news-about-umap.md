---
title: Some news about uMap!
date: 2023-08-25T13:24:19
source: https://www.openstreetmap.org/user/ybon/diary/402248
---

<p>Since a few month, uMap has been integrated in a French state incubator, so things are moving quite a lot!</p>

<p>uMap is now ten years old, and is deployed on many instances around the world. The one I know well is hosted by OSM France, and is close to reach one million maps created and 100.000 users.</p>

<p>This incubation program is ported by the French <a href="https://citoyens.transformation.gouv.fr/" rel="nofollow noopener noreferrer">“Accélérateur d’initiatives citoyennes”</a>, it includes coaches and a small budget for non tech needs (UI/UX…). One goal of this program is to find financial support for uMap development and maintainance. A French administration, the <a href="https://agence-cohesion-territoires.gouv.fr/" rel="nofollow noopener noreferrer">Agence pour la cohésion des territoires</a>, is the first uMap financial backer since a few months. This allowed us to put up a small team to work, part time, in uMap:</p>

<ul>
  <li>two devs: <a href="https://larlet.fr/david/" rel="nofollow noopener noreferrer">David Larlet</a> and myself</li>
  <li>a UX designer: <a href="http://www.marquedefabrique.net/" rel="nofollow noopener noreferrer">Aurélie Jallut</a></li>
  <li>a bizdev, which is yet to hire (interested ? Contact me!)</li>
</ul>

<p>That’s great news! Until then, uMap was 100% developed on my spare time.</p>

<p>uMap is used a lot by French public agents, and this explains the support from the French state, to make this tool better, and more official. For this, a first step is an “official” instance for public workers:</p>

<p><a href="https://umap.incubateur.anct.gouv.fr/fr/" rel="nofollow noopener noreferrer">https://umap.incubateur.anct.gouv.fr/fr/</a></p>

<p>We’ll be at the <a href="https://numerique-en-communs.fr/" rel="nofollow noopener noreferrer">NEC - Numérique En Communs</a> event (Bordeaux, France), on October 19th and 20th. See you there for more news and announcements!</p>

<h4 id="whats-new-in-umap-then-">What’s new in uMap, then ?</h4>

<p>First, a huge cleaning, upgrade and bug fight in uMap code. Since a few years, my time available for uMap has been very low (I’ve been a baker for two years…), so the code urgently needed more love.</p>

<p>What else? Here are a few of the notable changes made recently in uMap, let’s go!</p>

<h3 id="docker-image">Docker image</h3>

<p>Finally!</p>

<p><img alt="" src="https://i.imgur.com/AS29VPt.png" /></p>

<h3 id="custom-overlay">Custom overlay</h3>

<p><img alt="" src="https://i.imgur.com/qrjW5xA.png" /></p>

<h3 id="anonymous-edit-link-sent-by-email">Anonymous edit link sent by email</h3>

<p>After years of people losing their secret edit link, now there will be no more excuse…</p>

<p><img alt="" src="https://i.imgur.com/7UbMP7Y.png" /></p>

<h3 id="facet-search">Facet search</h3>

<p>Chose some properties, uMap will compute all the values and let people filter data.</p>

<p><img alt="" src="https://i.imgur.com/gmtiDSB.png" /></p>

<h3 id="permanent-credit">Permanent credit</h3>

<p>A credit that will display in the bottom left corner:</p>

<p><img alt="" src="https://i.imgur.com/qBRfZcd.png" /></p>

<h3 id="starred-maps">Starred maps</h3>

<p>A cool way to keep tracks of maps made by others!</p>

<p><img alt="" src="https://i.imgur.com/KRRW4Mg.png" /></p>

<h3 id="my-dashboard-page">My Dashboard page</h3>

<p>Very basic version of a dashboard page, where to retrieve all our maps, with more metadata and actions than the previous flat maps list.</p>

<p><img alt="" src="https://i.imgur.com/Fmmu65d.png" /></p>

<h3 id="better-control-of-default-map-view">Better control of default map view</h3>

<p><img alt="" src="https://i.imgur.com/iSFhkj0.png" /></p>

<h3 id="allow-to-edit-basic-user-profile-information">Allow to edit basic user profile information</h3>

<p>Useful for changing the username and adding more than one OAuth provider.</p>

<p><img alt="" src="https://i.imgur.com/nUN8R0G.png" /></p>

<h3 id="also">Also</h3>

<ul>
  <li>allow to control icon opacity</li>
  <li>allow to sort reverse (by adding a <code>-</code> before the property)</li>
  <li>allow to control links target: same window, new tab, parent window</li>
  <li>add Ctrl-Shift-click shortcut to edit features’s datalayer when clicking on the feature shape</li>
  <li>better natural sort of features</li>
  <li>allow non ascii chars in variables</li>
  <li>Make fromZoom and toZoom options available for all layers</li>
  <li>When map has max bounds set, use those bounds for limiting search</li>
</ul>

<p>Full <a href="https://umap-project.readthedocs.io/en/master/changelog/" rel="nofollow noopener noreferrer">changelog</a>.</p>

<h3 id="whats-next-">What’s next ?</h3>

<p>A user research session has started by Raywan (who helped us on the bizdev part) and Aurélie, targeted mainly on public workers, but not exclusively.</p>

<p>Some topics on the pipe, from this research and from long waited features:</p>

<ul>
  <li>concurrent live editing of a map</li>
  <li>define permissions at the datalayer level</li>
  <li>better icon management</li>
  <li>print a map</li>
  <li>UX revamp</li>
  <li>better user documentation</li>
  <li>teams?</li>
  <li>attachements support?</li>
</ul>

<p>uMap issue management is on Github: <a href="https://github.com/umap-project/umap/issues" rel="nofollow noopener noreferrer">https://github.com/umap-project/umap/issues</a>
Public roadmap is here: <a href="https://github.com/orgs/umap-project/projects/1/views/1" rel="nofollow noopener noreferrer">https://github.com/orgs/umap-project/projects/1/views/1</a></p>

<p>If you want to give your opinion on what should be done first, please add emojis in the issues list. And if something is missing from the list, please create new ones to share your ideas!</p>

<h3 id="how-to-contribute-">How to contribute ?</h3>

<p>It’s now finally possible to support uMap by donating on Liberapay or Open Collective. All amounts are welcome!</p>

<ul>
  <li><a href="https://liberapay.com/uMap/" rel="nofollow noopener noreferrer">https://liberapay.com/uMap/</a></li>
  <li><a href="https://opencollective.com/uMap" rel="nofollow noopener noreferrer">https://opencollective.com/uMap</a></li>
</ul>

<p>Help translating uMap in your language. uMap is available in 58 languages but many still lack between 15 to 20% to be complete.</p>

<p><a href="https://www.transifex.com/openstreetmap/umap/dashboard/" rel="nofollow noopener noreferrer">https://www.transifex.com/openstreetmap/umap/dashboard/</a></p>

<p>And of course, contribute to the code!</p>

<p><a href="https://github.com/umap-project/umap/" rel="nofollow noopener noreferrer">https://github.com/umap-project/umap/</a></p>

<p>Cheers, and thanks for your warm support since ten years!</p>
