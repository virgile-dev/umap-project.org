import os
from dataclasses import dataclass
from datetime import datetime
from html import escape
from pathlib import Path
from string import Template

import feedparser
import frontmatter
import httpx
import minicli
import mistune
from dotenv import load_dotenv
from jinja2 import Environment as Env
from jinja2 import FileSystemLoader
from selectolax.parser import HTMLParser
from slugify import slugify

DATA = Path() / "data"
STATIC = Path() / "static"
PUBLIC = Path() / "public"
TEMPLATES = Path() / "templates"

load_dotenv()
environment = Env(loader=FileSystemLoader(str(TEMPLATES)))
md = mistune.create_markdown(escape=False)

NORMALIZED_STRFTIME = "%Y-%m-%dT12:00:00+01:00"
TODAY = datetime.today()
BASE_URL = os.getenv("BASE_URL", "https://umap-project.org/")

article_template = Template(
    """\
---
title: "$title"
date: $date_published
source: $source
---

$html
"""
)


@minicli.cli
def import_article(url):
    page = httpx.get(url)
    tree = HTMLParser(page.text)
    rss_link = tree.css_first('link[type="application/rss+xml"]')
    rss_href = rss_link.attributes.get("href")
    feed = feedparser.parse(rss_href)
    for entry in feed["entries"]:
        if entry["link"] == url:
            break
    title = entry.get("title")
    summary = entry.get("summary")
    published_parsed = entry.get("published_parsed")
    date_published = datetime(*published_parsed[:6]).isoformat()
    article = article_template.substitute(
        title=title,
        date_published=date_published,
        source=url,
        html=summary,
    )
    (DATA / "blog" / f"{date_published[:10]}-{slugify(title)}.md").write_text(article)


@dataclass
class BlogPost:
    item: dict
    markdown: str
    html: str
    file_path: str

    def __post_init__(self):
        self.title = self.item["title"]
        self.title_escape = escape(self.title)
        self.date = self.item["date"]
        self.source = self.item["source"]
        self.slug = slugify(self.title)
        self.short_date = str(self.date)[:10]
        self.url = f"blog/{self.short_date}-{self.slug}/"
        self.full_url = f"{BASE_URL}{self.url}"
        self.normalized_date = self.date.strftime(NORMALIZED_STRFTIME)
        self.escaped_content = escape(self.html)

    def __eq__(self, other):
        return self.url == other.url

    def __lt__(self, other: "BlogPost"):
        if not isinstance(other, BlogPost):
            return NotImplemented
        return self.date < other.date

    @staticmethod
    def all(source: Path):
        items = []
        for markdown_file in each_file_from(source, pattern="*.md"):
            item = frontmatter.load(markdown_file)
            html = md(item.content)
            page = BlogPost(
                item=item,
                markdown=item.content,
                html=html,
                file_path=markdown_file.name,
            )
            items.append(page)
        return sorted(items, reverse=True)


@minicli.cli
def build():
    common_kwargs = {"base_url": BASE_URL}

    template_index = environment.get_template("index.html")
    content = template_index.render(lang="en", nav_selected="home", **common_kwargs)
    (PUBLIC / "index.html").write_text(content)

    template_index = environment.get_template("index-fr.html")
    content = template_index.render(lang="fr", nav_selected="accueil", **common_kwargs)
    target_path = PUBLIC / "fr"
    target_path.mkdir(exist_ok=True)
    (target_path / "index.html").write_text(content)

    template_blog = environment.get_template("blog.html")
    blogposts = BlogPost.all(DATA / "blog")
    content = template_blog.render(
        blogposts=blogposts, lang="en", nav_selected="blog", **common_kwargs
    )
    target_path = PUBLIC / "blog"
    target_path.mkdir(exist_ok=True)
    (target_path / "index.html").write_text(content)
    for blogpost in blogposts:
        content = template_blog.render(
            blogposts=[blogpost], lang="en", nav_selected="blog", **common_kwargs
        )
        target_path = PUBLIC / "blog" / f"{blogpost.short_date}-{blogpost.slug}"
        target_path.mkdir(exist_ok=True)
        (target_path / "index.html").write_text(content)


@minicli.cli
def feed():
    """Generate a feed from last published items."""
    template = environment.get_template("feed.xml")

    # On récupère toutes les blogposts.
    blogposts = BlogPost.all(DATA / "blog")

    content = template.render(
        blogposts=blogposts,
        current_dt=TODAY.strftime(NORMALIZED_STRFTIME),
        BASE_URL=BASE_URL,
    )
    target_path = PUBLIC / "blog"
    target_path.mkdir(exist_ok=True)
    (target_path / "feed.atom").write_text(content)


def each_file_from(source_dir, pattern="*", exclude=None):
    """Walk across the `source_dir` and return the `pattern` file paths."""
    for path in _each_path_from(source_dir, pattern=pattern, exclude=exclude):
        if path.is_file():
            yield path


def _each_path_from(source_dir, pattern="*", exclude=None):
    for path in sorted(Path(source_dir).glob(pattern)):
        if exclude is not None and path.name in exclude:
            continue
        yield path


if __name__ == "__main__":
    minicli.run()
