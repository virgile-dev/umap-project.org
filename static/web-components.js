class UsersCount extends HTMLElement {
  static register(tagName = 'users-count') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://umap-project.gitlab.io/umap-stats/data.json')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw response
      })
      .then(
        (data) =>
          (this.textContent = new Intl.NumberFormat(
            document.querySelector('html').lang
          ).format(data.users_count))
      )
      .catch((error) => console.debug(error))
  }
}

UsersCount.register()

class MapsCount extends HTMLElement {
  static register(tagName = 'maps-count') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://umap-project.gitlab.io/umap-stats/data.json')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw response
      })
      .then(
        (data) =>
          (this.textContent = new Intl.NumberFormat(
            document.querySelector('html').lang
          ).format(data.maps_count))
      )
      .catch((error) => console.debug(error))
  }
}

MapsCount.register()

class MapsActiveCount extends HTMLElement {
  static register(tagName = 'maps-active-count') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://umap-project.gitlab.io/umap-stats/data.json')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw response
      })
      .then(
        (data) =>
          (this.textContent = new Intl.NumberFormat(
            document.querySelector('html').lang
          ).format(data.maps_active_count))
      )
      .catch((error) => console.debug(error))
  }
}

MapsActiveCount.register()

class LastRelease extends HTMLElement {
  static register(tagName = 'last-release') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://api.github.com/repos/umap-project/umap/releases/latest')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw response
      })
      .then((data) => (this.textContent = data.name))
      .catch((error) => console.debug(error))
  }
}

LastRelease.register()

class LastCommit extends HTMLElement {
  static register(tagName = 'last-commit') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://api.github.com/repos/umap-project/umap/commits?per_page=1')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw response
      })
      .then((data) => (this.textContent = data[0].commit.author.date))
      .catch((error) => console.debug(error))
  }
}

LastCommit.register()

class StargazersCount extends HTMLElement {
  static register(tagName = 'stargazers-count') {
    if ('customElements' in window && !customElements.get(tagName)) {
      customElements.define(tagName, this)
    }
  }

  connectedCallback() {
    fetch('https://api.github.com/repos/umap-project/umap')
      .then((response) => {
        if (response.ok) {
          return response.json()
        }
        throw response
      })
      .then((data) => (this.textContent = data.stargazers_count))
      .catch((error) => console.debug(error))
  }
}

StargazersCount.register()
