# umap-project.org

## Installation

Pré-requis : Python3

Installer et activer un environnement virtuel :

    $ python3 -m venv venv
    $ source venv/bin/activate

Installer les dépendances :

    $ make install

## Lancement

Lancer le serveur local :

    $ make build serve

Aller sur http://127.0.0.1:8000/

## Importer un article

    $ make import_article url=https://www.openstreetmap.org/user/ybon/diary/402589
